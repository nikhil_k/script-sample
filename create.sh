#!/bin/bash
function main
{
    init "$@"
    

}

function usage
{
  cat <<EOF

  Usage:
      -t target: Valid options are staging, dev and prod. 
  
  Examples:      
      For building the image:
      ./deploy.sh -t staging/prod/dev

EOF
}

function init
{

# Default Values

TARGET=""
#getopts module is used to sett parametrized options
while getopts h:t: option;
  do
    case "${option}" in
    h) usage "";exit 1
     ;;

    t)
     TARGET=$OPTARG
     ;; #-t for action. Valid values are staging, productions and development

    \?) usage "";exit 1
     ;;
        esac
  done
}

main "$@"